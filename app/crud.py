from sqlalchemy.orm import Session

from models import OperationModel


def get_history(db: Session):

    return db.query(OperationModel).all()


def create_operation(db: Session, operation: str, result: float):
    db_operation = OperationModel(operation=operation, result=result)
    db.add(db_operation)
    db.commit()
    db.refresh(db_operation)
    return db_operation
