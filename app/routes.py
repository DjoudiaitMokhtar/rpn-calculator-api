import openpyxl

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import StreamingResponse
from io import BytesIO
from sqlalchemy.orm import Session
from rpn_calculator.rpn_calculator import RPNCalculator
from rpn_calculator.rpn_calculator import (
    UnknownOperationError,
    TooFewArgumentError,
    IncompleteFormulaError,
)

from crud import create_operation, get_history
from schemas import OperationBase, Operation
from models import get_db, OperationModel

router = APIRouter()


@router.get("/", tags=["Welcome to my RPI calculator"])
def root():
    return {"message": "Welcome to my RPI calculator"}


@router.post(
    "/operations/",
    response_model=Operation,
    summary="Perform Arithmetic Operation",
    description="This route performs an arithmetic operation and adds the result to the history.",
    tags=["Arithmetic Operations"],
)
def save_operation(operation: str, db: Session = Depends(get_db)):
    try:
        result = RPNCalculator().evaluate(operation)
        return create_operation(db=db, operation=operation, result=result)
    except UnknownOperationError:
        HTTPException(status_code=400, detail="Unknown operation")
    except TooFewArgumentError:
        HTTPException(status_code=400, detail="Too Few Argument")
    except IncompleteFormulaError:
        HTTPException(status_code=400, detail="Incomplete Formula")
    except RuntimeError:
        HTTPException(status_code=400, detail="Should never happend")


@router.get(
    "/operations/export-history",
    summary="Retrieve operations History",
    description="This route retrieves the history of valid arithmetic operations exports it to a CSV file.",
    response_class=StreamingResponse,
    tags=["Operations History"],
)
def operation_history(db: Session = Depends(get_db)):
    operations_history = get_history(db=db)
    workbook = openpyxl.Workbook()
    sheet = workbook.active
    headers = [
        column.name
        for column in OperationModel.__table__.columns
        if column.name != "id"
    ]
    sheet.append(headers)

    for instance in operations_history:
        row_data = [
            getattr(instance, column.name)
            for column in OperationModel.__table__.columns
            if column.name != "id"
        ]
        sheet.append(row_data)

    excel_buffer = BytesIO()
    workbook.save(excel_buffer)
    excel_buffer.seek(0)
    content_disposition = f'attachment; filename="export.xlsx"'
    return StreamingResponse(
        excel_buffer,
        media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        headers={"Content-Disposition": content_disposition},
    )
