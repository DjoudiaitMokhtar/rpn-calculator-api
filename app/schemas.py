from pydantic import BaseModel


class OperationBase(BaseModel):
    operation: str


class Operation(OperationBase):
    id: int
    operation: str
    result: float

    class Config:
        from_orm = True
