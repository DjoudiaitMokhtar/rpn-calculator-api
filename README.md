# RPN Calculator API
RPN Calculator is an API for performing calculations using RPN inverse Polish notation. If the formula is valid, the result and operation are stored in the database.
## Quick usage
***
A little intro about the installation. 
```
$ git clone https://gitlab.com/-/ide/project/DjoudiaitMokhtar/rpn-calculator-api/
$ pip install virtualenv # Virtualenv is a tool to create isolated Python environments.
$ source venv/bin/activate # Launch the environment
$ You can find the api documentation and test it by clicking on this link http://127.0.0.1:8000/docs
You can also use the docker image directly :
$ docker compose build
$ docker compose up
```